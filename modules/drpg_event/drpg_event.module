<?php

/**
 * @file
 * Defines event functionality.
 *
 * Events are processed to update the user's game state.
 */

/**
 * Implements hook_menu().
 */
function drpg_event_menu() {
  $items = array();

  $items['drpg/process/events'] = array(
    'title' => 'DRPG Process Events',
    'description' => 'Process event data.',
    'page callback' => 'drpg_event_process_events',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'includes/drpg_event.process.inc',
    'weight' => 0,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function drpg_event_permission() {
  return array(
    'process drpg event' => array(
      'title' => t('Process DRPG event'),
      'description' => (t('Process an event defined by the hook_drpg_event_method_info hook.')),
    ),
  );
}

/**
 * Implements hook_hook_info().
 */
function drpg_event_hook_info() {
  $hooks = array(
    'drpg_event_method_info' => array(
      'group' => 'drpg',
    ),
  );

  return $hooks;
}

/**
 * Returns an array of methods that can be used to process game events.
 *
 * @see hook_drpg_event_method_info()
 *
 * @return array
 *   Array of event method information.
 */
function drpg_event_methods() {
  $event_methods = &drupal_static(__FUNCTION__);

  if (!isset($event_methods)) {
    $event_methods = array();

    foreach (module_implements('drpg_event_method_info') as $module) {
      foreach (module_invoke($module, 'drpg_event_method_info') as $method_id => $method_info) {
        $event_methods[$method_id] = $method_info;
      }
    }
  }

  return $event_methods;
}

/**
 * Access callback for game event processing.
 */
function drpg_event_can_process_event() {
  global $user;

  return user_access('process drpg event', $user);
}
