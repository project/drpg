<?php

/**
 * @file
 * Defines the schema for the attribute entity.
 */

/**
 * Implements hook_schema().
 */
function drpg_attribute_schema() {
  $schema = array();

  $schema['drpg_attribute'] = array(
    'description' => 'The base table for the attribute entities.',
    'fields' => array(
      'attribute_id' => array(
        'description' => 'The primary identifier for an attribute.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => 'The type of entity this attribute is attached to.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'entity_id' => array(
        'description' => 'The entity ID of this attribute\'s owner.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'attribute_name' => array(
        'description' => 'The name of this attribute.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'value' => array(
        'description' => 'The value of this attribute.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when this attribute was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'updated' => array(
        'description' => 'The Unix timestamp when this attribute was last saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'drpg_attribute_updated' => array('updated'),
      'drpg_attribute_created' => array('created'),
      'drpg_attribute_entity' => array('type', 'entity_id'),
      'drpg_attribute_name' => array('type', 'entity_id', 'attribute_name'),
    ),
    'primary key' => array('attribute_id'),
  );

  return $schema;
}
