<?php

/**
 * @file
 * Data response class for NPC data.
 */

class DrpgNPCResponse extends DrpgDataResponse {
  public $npc;

  /**
   * Class constructor.
   */
  public function __construct() {
    parent::__construct();
  }
}
