<?php

/**
 * @file
 * Defines avatar functionality.
 */

/**
 * Implements hook_entity_info().
 */
function drpg_avatar_entity_info() {
  return array(
    'drpg_avatar' => array(
      'label' => t('Avatar'),
      'plural label' => t('Avatars'),
      'controller class' => 'DrpgAvatarEntityController',
      'rules controller class' => 'EntityDefaultRulesController',
      'entity class' => 'DrpgAvatar',
      'base table' => 'drpg_avatar',
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => 'avatar_id',
        'label' => 'label',
      ),
      // Enable the entity API's admin UI.
      'admin ui' => array(
        'path' => 'admin/drpg/avatars',
        'file' => 'drpg_avatar.admin.inc',
        'file path' => drupal_get_path('module', 'drpg_avatar') . '/includes',
        'controller class' => 'DrpgAvatarUIController',
      ),
      'uri callback' => 'entity_class_uri',
      'module' => 'drpg_avatar',
      'label callback' => 'entity_class_label',
      'access callback' => 'drpg_avatar_access',
    ),
  );
}

/**
 * Access callback for avatar admin functionality.
 */
function drpg_avatar_access($op, $entity, $account = NULL) {
  return user_access('administer drpg', $account);
}

/**
 * Implements hook_menu().
 */
function drpg_avatar_menu() {
  $items = array();

  $items['drpg/data/avatar'] = array(
    'title' => 'DRPG Avatar',
    'description' => 'Get avatar data.',
    'page callback' => 'drpg_avatar_data',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'includes/drpg_avatar.data.inc',
    'weight' => 0,
  );

  return $items;
}

/**
 * Implements hook_drpg_event_method_info().
 */
function drpg_avatar_drpg_event_method_info() {
  $events = array();

  $events['drpg_avatar_set_name_event'] = array(
    'description' => 'Set the name of the currently active avatar.',
    'access' => 'drpg_event_can_process_event',
  );

  return $events;
}

/**
 * Implements hook_drpg_attribute_names().
 */
function drpg_avatar_drpg_attribute_names() {
  $attribute_names = array(
    'hp' => 'HP',
    'xp' => 'XP',
    'attack' => 'Attack',
  );

  return $attribute_names;
}

/**
 * Creates a default Avatar for a given user.
 *
 * @param int $uid
 *   The user ID to create an avatar for.
 */
function drpg_avatar_create_default($uid) {
  $values = array(
    'owner_uid' => $uid,
    'label' => 'Avatar',
  );

  $avatar = new DrpgAvatar($values);
  $avatar->save();
}

/**
 * Returns the active avatar for the a given user.
 *
 * @param int $uid
 *   The user ID of the avatar owner.
 *
 * @return DrpgAvatar
 *   The user's current avatar.
 */
function drpg_avatar_get_user_active_avatar($uid) {
  return current(drpg_avatar_get_user_avatars($uid));
}

/**
 * Returns all avatars for a given user.
 *
 * @param int $uid
 *   The user ID of the avatar owner.
 *
 * @return array
 *   Array of DrpgAvatar objects.
 */
function drpg_avatar_get_user_avatars($uid) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'drpg_avatar')
    ->propertyCondition('owner_uid', $uid, '=');

  $result = $query->execute();

  $avatars = array();

  if (isset($result['drpg_avatar'])) {
    $entity_ids = array_keys($result['drpg_avatar']);
    $avatars = entity_load('drpg_avatar', $entity_ids);
  }

  foreach ($avatars as $index => $avatar) {
    $avatars[$index]->attributes = drpg_attribute_get_entity_attributes('drpg_avatar', $avatar->avatar_id);
  }

  return $avatars;
}

/**
 * Sets the name of the currently active avatar.
 *
 * @param string $name
 *   The avatar name to set.
 *
 * @return bool
 *   TRUE if avatar name is successfully set.
 */
function drpg_avatar_set_name_event($name) {
  global $user;

  $avatar = drpg_avatar_get_user_active_avatar($user->uid);
  $avatar->label = $name;

  return ($avatar->save() !== FALSE);
}
