<?php

/**
 * @file
 * Data response class for avatar data.
 */

class DrpgAvatarResponse extends DrpgDataResponse {
  public $avatar;

  /**
   * Class constructor.
   */
  public function __construct() {
    parent::__construct();
  }
}
