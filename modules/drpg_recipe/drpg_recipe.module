<?php

/**
 * @file
 * Defines recipe functionality.
 *
 * Recipes can be used to combine Items to create other Items.
 */

/**
 * Implements hook_entity_info().
 */
function drpg_recipe_entity_info() {
  $entities = array();

  $entities['drpg_recipe'] = array(
    'label' => t('Recipe'),
    'plural label' => t('Recipes'),
    'controller class' => 'DrpgRecipeEntityController',
    'rules controller class' => 'EntityDefaultRulesController',
    'entity class' => 'DrpgRecipe',
    'base table' => 'drpg_recipe',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'recipe_id',
      'label' => 'label',
    ),
    // Enable the entity API's admin UI.
    'admin ui' => array(
      'path' => 'admin/drpg/recipes',
      'file' => 'drpg_recipe.admin.inc',
      'file path' => drupal_get_path('module', 'drpg_recipe') . '/includes',
      'controller class' => 'DrpgRecipeUIController',
    ),
    'uri callback' => 'entity_class_uri',
    'module' => 'drpg_recipe',
    'label callback' => 'entity_class_label',
    'access callback' => 'drpg_recipe_access',
  );

  $entities['drpg_recipe_item'] = array(
    'label' => t('Recipe Item'),
    'plural label' => t('Recipe Items'),
    'controller class' => 'DrpgRecipeItemEntityController',
    'rules controller class' => 'EntityDefaultRulesController',
    'entity class' => 'DrpgRecipeItem',
    'base table' => 'drpg_recipe_item',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'recipe_item_id',
      'label' => 'label',
    ),
    'uri callback' => 'entity_class_uri',
    'module' => 'drpg_recipe',
    'label callback' => 'entity_class_label',
    'access callback' => 'drpg_recipe_access',
  );

  return $entities;
}

/**
 * Access callback for recipe admin functionality.
 */
function drpg_recipe_access($op, $entity, $account = NULL) {
  return user_access('administer drpg', $account);
}

/**
 * Implements hook_drpg_event_method_info().
 */
function drpg_recipe_drpg_event_method_info() {
  $events = array();

  $events['drpg_recipe_create_recipe_item_event'] = array(
    'description' => 'Creates an item using a recipe.',
    'access' => 'drpg_event_can_process_event',
  );

  return $events;
}

/**
 * Returns a recipe for a given recipe ID.
 *
 * @param int $recipe_id
 *   The recipe ID.
 *
 * @return DrpgRecipe
 *   The recipe.
 */
function drpg_recipe_get_recipe($recipe_id) {
  return entity_load_single('drpg_recipe', $recipe_id);
}

/**
 * Returns an array of recipe items for a given recipe ID.
 *
 * @param int $recipe_id
 *   The ID of the recipe to return recipe items for.
 *
 * @return array
 *   Array of DrpgRecipeItem objects.
 */
function drpg_recipe_get_recipe_items($recipe_id) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'drpg_recipe_item')
    ->propertyCondition('recipe_id', $recipe_id, '=');

  $result = $query->execute();

  $recipe_items = array();

  if (isset($result['drpg_recipe_item'])) {
    $entity_ids = array_keys($result['drpg_recipe_item']);
    $recipe_items = entity_load('drpg_recipe_item', $entity_ids);
  }

  return array_values($recipe_items);
}

/**
 * Returns a recipe item for a given recipe item ID.
 *
 * @param int $recipe_item_id
 *   The recipe item ID.
 *
 * @return DrpgRecipeItem
 *   The recipe item.
 */
function drpg_recipe_get_recipe_item($recipe_item_id) {
  return entity_load_single('drpg_recipe_item', $recipe_item_id);
}

/**
 * Creates an item using a recipe.
 *
 * @param string $recipe_id
 *   The ID of the recipe to use.
 *
 * @return bool
 *   TRUE if an item is successfully created using the recipe.
 */
function drpg_recipe_create_recipe_item_event($recipe_id) {
  global $user;

  $recipe = drpg_recipe_get_recipe($recipe_id);

  if (!$recipe) {
    return FALSE;
  }

  $recipe_wrapper = entity_metadata_wrapper('drpg_recipe', $recipe);

  $item = drpg_item_get_item($recipe_wrapper->drpg_item_ref->value()->item_id);

  if (!$item) {
    return FALSE;
  }

  $recipe_items = drpg_recipe_get_recipe_items($recipe_id);
  if (empty($recipe_items)) {
    return FALSE;
  }

  $avatar = drpg_avatar_get_user_active_avatar($user->uid);
  $inventory = drpg_inventory_item_get_inventory('drpg_avatar', $avatar->avatar_id);

  $inventory_items = array();

  // Create an index of all inventory items.
  foreach ($inventory['item_instances'] as $item_instance) {
    if (!isset($inventory_items[$item_instance->item_id])) {
      $inventory_items[$item_instance->item_id] = array();
    }

    $inventory_items[$item_instance->item_id][] = $item_instance->inventory_item_id;
  }

  // Check inventory item quantities are sufficient for the recipe.
  foreach ($recipe_items as $recipe_item) {
    if (!isset($inventory_items[$recipe_item->item_id]) || count($inventory_items[$recipe_item->item_id]) < $recipe_item->quantity) {
      // Not enough inventory items to complete this recipe.
      return FALSE;
    }
  }

  // Remove inventory items required by the recipe.
  foreach ($recipe_items as $recipe_item) {
    for ($i = 0; $i < $recipe_item->quantity; $i++) {
      drpg_inventory_item_delete_item($inventory_items[$recipe_item->item_id][$i]);
    }
  }

  // Create the recipe item.
  drpg_inventory_item_create_item('drpg_avatar', $avatar->avatar_id, $item->item_id);

  return TRUE;
}
