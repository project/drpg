<?php

/**
 * @file
 * Defines the schema for the room entity.
 */

/**
 * Implements hook_schema().
 */
function drpg_room_schema() {
  $schema = array();

  $schema['drpg_room'] = array(
    'description' => 'The base table for the room entities.',
    'fields' => array(
      'room_id' => array(
        'description' => 'The primary identifier for a room.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'The label of this room.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'item_container_quantity' => array(
        'description' => 'The number of item containers this room holds.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when this room was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'updated' => array(
        'description' => 'The Unix timestamp when this room was last saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('room_id'),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function drpg_room_install() {
  // Attach multiple item container reference field to room entity.
  $instance = array(
    'field_name' => 'drpg_multi_item_container_ref',
    'entity_type' => 'drpg_room',
    'bundle' => 'drpg_room',
    'label' => 'Item Containers',
    'required' => FALSE,
    'widget' => array(
      'type' => 'options_select',
    ),
    'settings' => array(
      'target_type' => 'drpg_item_container',
      'handler_settings' => array('target_bundles' => NULL),
    ),
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'type' => 'entityreference_label',
      ),
    ),
  );

  field_create_instance($instance);



  // Attach NPC entity reference field to room entity.
  $instance = array(
    'field_name' => 'drpg_multi_npc_ref',
    'entity_type' => 'drpg_room',
    'bundle' => 'drpg_room',
    'label' => 'NPCs',
    'required' => FALSE,
    'widget' => array(
      'type' => 'options_select',
    ),
    'settings' => array(
      'target_type' => 'drpg_npc',
      'handler_settings' => array('target_bundles' => NULL),
    ),
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'type' => 'entityreference_label',
      ),
    ),
  );

  field_create_instance($instance);
}

