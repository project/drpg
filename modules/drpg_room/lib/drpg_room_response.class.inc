<?php

/**
 * @file
 * Data response class for room data.
 */

class DrpgRoomResponse extends DrpgDataResponse {
  public $room;

  /**
   * Class constructor.
   */
  public function __construct() {
    parent::__construct();
  }
}
