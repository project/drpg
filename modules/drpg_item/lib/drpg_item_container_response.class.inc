<?php

/**
 * @file
 * Data response class for item container data.
 */

class DrpgItemContainerResponse extends DrpgDataResponse {
  public $itemContainer;

  /**
   * Class constructor.
   */
  public function __construct() {
    parent::__construct();
  }
}
