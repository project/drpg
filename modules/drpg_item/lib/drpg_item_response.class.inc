<?php

/**
 * @file
 * Data response class for item data.
 */

class DrpgItemResponse extends DrpgDataResponse {
  public $items;

  /**
   * Class constructor.
   */
  public function __construct() {
    parent::__construct();
  }
}
