<?php

/**
 * @file
 * Defines the schema for the item entity.
 */

/**
 * Implements hook_schema().
 */
function drpg_item_schema() {
  $schema = array();

  $schema['drpg_item'] = array(
    'description' => 'The base table for item entities.',
    'fields' => array(
      'item_id' => array(
        'description' => 'The primary identifier for an item.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'The label of this item.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'rarity' => array(
        'description' => 'The rarity of this item.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'equippable' => array(
        'description' => 'Boolean indicating whether this item is equippable.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'currency_cost' => array(
        'description' => 'The currency cost of this item.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'currency_value' => array(
        'description' => 'The currency value of this item.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'ui_asset_path' => array(
        'description' => 'The path of this item\'s UI asset.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'game_asset_path' => array(
        'description' => 'The path of this item\'s in-game asset.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'created' => array(
        'description' => 'The Unix timestamp when this item was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'updated' => array(
        'description' => 'The Unix timestamp when this item was last saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'drpg_item_rarity' => array('rarity'),
    ),
    'primary key' => array('item_id'),
  );

  $schema['drpg_item_container'] = array(
    'description' => 'The base table for item container entities.',
    'fields' => array(
      'item_container_id' => array(
        'description' => 'The primary identifier for an item container.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'The label of this item container.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'item_quantity' => array(
        'description' => 'The quantity of items within this item container.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'item_rarity' => array(
        'description' => 'The rarity of items within this item container.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'item_rarity_operator' => array(
        'description' => 'The operator used with the item rarity.',
        'type' => 'varchar',
        'length' => 2,
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when this item container was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'updated' => array(
        'description' => 'The Unix timestamp when this item container was last saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('item_container_id'),
  );

  $schema['drpg_item_instance'] = array(
    'description' => 'The base table for item instance entities.',
    'fields' => array(
      'item_instance_id' => array(
        'description' => 'The primary identifier for an item instance.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => 'The type of this item instance.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'entity_id' => array(
        'description' => 'The entity ID this item instance was created for.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'item_id' => array(
        'description' => 'The item ID this item instance represents.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when this item instance was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'updated' => array(
        'description' => 'The Unix timestamp when this item instance was last saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'drpg_item_instance' => array('type', 'entity_id', 'item_id'),
    ),
    'primary key' => array('item_instance_id'),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function drpg_item_install() {
  // Create entity reference field for items.
  $field = array(
    'field_name' => 'drpg_item_ref',
    'type' => 'entityreference',
    'settings' => array(
      'target_type' => 'drpg_item',
      'handler_settings' => array('target_bundles' => NULL),
    ),
    'cardinality' => 1,
    'translatable' => FALSE,
  );

  field_create_field($field);

  // Create entity reference field for multiple item containers.
  $field = array(
    'field_name' => 'drpg_multi_item_container_ref',
    'type' => 'entityreference',
    'settings' => array(
      'target_type' => 'drpg_item_container',
      'handler_settings' => array('target_bundles' => NULL),
    ),
    'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    'translatable' => FALSE,
  );

  field_create_field($field);
}

/**
 * Implements hook_uninstall().
 */
function drpg_item_uninstall() {
  field_delete_field('drpg_item_ref');
  field_delete_field('drpg_multi_item_container_ref');
}
